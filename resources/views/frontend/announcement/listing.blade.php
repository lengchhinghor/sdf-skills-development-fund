@extends('frontend.layout.master') 
@section('title', 'Career Detail') 
@section('active-news', 'active') 
@section('content')

        @if($banner)
    <div class="page-header" style="background-image: url({{ asset ($banner->image)}});">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li><a href="#">{{__('general.announcement')}}</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
        @endif

    <div class="wrapper-content bg-white container">
        
        <div class="row">
            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                <div class="about-section news-detail-cnt container ">

                    {{-- ============================== Page Header ================================ --}}
                    <div class="row">
                        <div class="col-xl-12 director-title-top mb-4">
                            <h3 class="">{{__('general.announcement')}}</h3>
                        </div>
                    </div>

                    <div class="row ">
                        {{-- ============================== Career Detail ================================ --}}
                        <div class=" col-12 mb10">

                          @if( count($data) > 0 )
                            @foreach($data as $row)
                            <div class="announcement-item  ">
                                 <i class="fa fa-bullhorn"  aria-hidden="true"></i> {{ $row->title}} 

                                <span class="date float-right">
                                    <i class="fa fa-calendar"  aria-hidden="true"></i> {{ date("d M Y", strtotime($row->created_at)) }}  &nbsp; 
                                <a href="{{$row->file}}"><i class="fa fa-download"  aria-hidden="true"></i></a>
                                </span>
                            </div>
                            @endforeach
                             @else
                            <div class="col-12">
                                <div class="alert alert-warning" role="alert">
                                    គ្មាន​ទិន្នន័យ
                                </div>
                            </div>
                            @endif
                        </div>     
                    </div>

                </div>
            </div> 

            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 sidebar-cnt">
                <div class="sidebar-title">
                    <h3>{{__('general.news-and-events')}}</h3>
                </div>
                <div class="sidebar-listing">
                    <ul class="list-group">
                        <li class="list-group-item"> <a href=" {{ route('news', $locale) }}">{{__('general.news')}}</a> </li>
                        <li class="list-group-item"><a href="{{ route('announcement', $locale) }}">{{__('general.announcement')}}</a> </li>
                        <li class="list-group-item"><a href="{{ route('event', $locale) }}">{{__('general.events')}}</a> </li>
                        <li class="list-group-item"><a href="{{ route('career', $locale) }}">{{__('general.careers')}}</a> </li>
                    </ul>
                </div>
            </div>
        </div>

        {{-- ============================== Pagination ================================ --}}
        <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center ">
                    <br />
                    <div class="st-pagination d-flex justify-content-center">
                        <ul class="pagination">
                        {{ $data->links('vendor.pagination.frontend-html') }}
                        </ul>
                    </div>
                </div>
            </div>
    </div>
 
@endsection