@extends('frontend.layout.master') 
@section('title', 'Career Detail') 
@section('active-news', 'active') 
@section('content')

        @if($banner)
    <div class="page-header" style="background-image: url({{ asset ($banner->image)}});">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li><a href="#">{{__('news-and-events')}}</a></li>
                            <li class="active">{{__('general.careers')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
        @endif
    <div class="wrapper-content bg-white container">
        
        <div class="row">
            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                <div class="about-section news-detail-cnt container ">

                    {{-- ============================== Page Header ================================ --}}
                    <div class="row">
                        <div class="col-xl-12 director-title-top mb-4">
                            <h3 class="">{{ __('general.careers') }}</h3>
                        </div>
                    </div>

                    <div class="row ">
                        {{-- ============================== Career Detail ================================ --}}
                        <div class=" col-12 mb10">

                            {{-- ============================== Career Detail ================================ --}}
                            <h4  >{!! $data->title ?? '' !!}</h4>
                            <div class="features col-12">
                                <span class="date"><i class="fa fa-calendar" style=" color: #006cb9;" aria-hidden="true"></i> February 9, 2017</span>

                                <span class="date float-right">
                                    <i class="fab fa-facebook-f" style=" color: #006cb9;" aria-hidden="true"></i> &nbsp; 
                                    <i class="fas fa-print" style=" color: #006cb9;" aria-hidden="true"></i>
                                </span>
                            </div>
                            
                        </div>

                        <div class=" col-12 mb10">

                            <h4>Job Description</h4>
                           <p>{{$data->description}}</p>
                           
                            <p>{!! $data->content !!}</p>
                        </div>

                        <hr />
                        
                        {{-- ============================== Application Form ================================ --}}
                        <div class=" col-12  ">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col " >
                                        <h4> Submit Application Form</h4>
                                    </div>
                                    <div class="col-12">
                                    @if(Session::has('msg'))
                                        <div class="alert alert-success" role="alert">
                                            {{ Session::get('msg') }}
                                        </div>
                                        @endif @if (count($errors) > 0)
                                        <div class="alert alert-danger" role="alert">
                                            Sending fail! Please Try again!
                                        </div>
                                        @endif @if (count($errors) > 0) @foreach ($errors->all() as $error)
                                        <div class="alert alert-danger" role="alert">
                                            {{ $error }}
                                        </div>
                                    @endforeach @endif
                                    </div>
                                </div>
                                  <form class="contact-us" method="post" action="{{ route('send-career') }}" enctype="multipart/form-data">
                                    {{ csrf_field() }} {{ method_field('PUT') }}
                                <div class="row">
                                    <div class="col-xs-12" >
                                        <div class="form-group">
                                            <input id="career_id" name="career_id" type="text" hidden value="{{$data->id}}" >
                                        </div>
                                    </div>

                                     <div class="col-xs-12 col-sm-4 col-md-4" >
                                        <div class="form-group">
                                            <label class="sr-only control-label" for="name">{{__('general.name')}}<span class=" "> </span></label>
                                            <input id="name" name="name" type="text" placeholder="{{__('general.name')}}*" class="form-control input-md">
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-4 col-md-4" >
                                        <div class="form-group">
                                            <label class="sr-only control-label" for="name">{{__('general.phone')}}<span class=" "> </span></label>
                                            <input id="phone" name="phone" type="text" placeholder="{{__('general.phone')}}*" class="form-control input-md">
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-4 col-md-4" >
                                        <div class="form-group">
                                            <label class="sr-only control-label" for="name">{{__('general.email')}}<span class=" "> </span></label>
                                            <input id="email" name="email" type="text" placeholder="{{__('general.email')}}" class="form-control input-md">
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-12" >
                                        <div class="form-group">
                                           <input  id="file"  name="file" type="file"  value="file">
                                        <!-- <span class="info"> pdf, doc, docx, jpeg, gif, zip</span> --><br />
                                            Only PDF file is accepted and maximum 1M.
                                        </div>
                                           
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6" >
                                            <script src="https://www.google.com/recaptcha/api.js" async defer></script>
                                            <div class="g-recaptcha" data-sitekey="6LdBDbMUAAAAAHKzSXVrtACFD5qBgeXyGHkvsTGy"></div>
                                               
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 text-right" >
                                        <div class="form-group">
                                                <button type="submit" class="btn btn-default">{{__('general.submit')}}</button>
                                        </div>  
                                    </div>

                                </div>
                                 </form>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div> 

            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 sidebar-cnt">
                <div class="sidebar-title">
                    <h3>{{__('general.news-and-events')}}</h3>
                </div>
                <div class="sidebar-listing">
                    <ul class="list-group">
                        <li class="list-group-item"> <a href=" {{ route('news', $locale) }}">{{__('general.news')}}</a> </li>
                        <li class="list-group-item"><a href="{{ route('announcement', $locale) }}">{{__('general.announcement')}}</a> </li>
                        <li class="list-group-item"><a href="{{ route('event', $locale) }}">{{__('general.events')}}</a> </li>
                        <li class="list-group-item"><a href="{{ route('career', $locale) }}">{{__('general.careers')}}</a> </li>
                    </ul>
                </div>
            </div>
        </div>
 
    </div>
 
@endsection