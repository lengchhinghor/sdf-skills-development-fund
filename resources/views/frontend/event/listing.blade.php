@extends('frontend.layout.master') 
@section('title', 'Welcome to SDF') 
@section('active-news', 'active')


@section('content')

        @if($banner)
    <div class="page-header" style="background-image: url({{ asset ($banner->image)}});">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li><a href="#">{{__('general.news-and-events')}}</a></li>
                            <li class="active">{{__('general.events')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
        @endif

    <div class="wrapper-content bg-white container">

        <div class="row">
            <div class="col-xl-12">
                <div class="about-section news-detail-cnt ">

                    {{-- ============================== Page Header ================================ --}}
                    <div class="row">
                        <div class="col-xl-12 director-title-top mb-4">
                            <div class="title">
                                <h3 class="">{{__('general.events')}}</h3>
                            </div>
                        </div>
                    </div>

                    {{-- ============================== Event Items ================================ --}}
                    {{-- <div class="row ">
                        <div class="container-fluid">
                            <div class="row news-items ">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12  ">
                                        <img src="{{ asset('public/frontend/camcyber/news/3.jpg') }}" alt="">
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                    
                                    <h3> Foundation Real Estate Agent in Cambodia </h3>
                                    <div class="event-listing" style="border-top: 1px solid #f4f6f8;">
                                            <i class="icon-calendar-3  "></i><span>&nbsp;  February 9, 2017</h3></span>
                                    </div>
                                    <div class="event-listing">
                                        <i class="icon-clock  "></i>&nbsp; <span>08​ : 00 am - 05 : 00 pm</h3></span>
                                    </div>
                                   
                                    <div class="event-listing">
                                        <i class="icon-placeholder-3  "></i>&nbsp;  <span>សណ្ធាគាភ្នំពេញ ផ្លូវលេខ៩២ រុក្ខវិថីដូនពេញ សង្កាត់វត្តភ្នំ ខ័ណ្ឌ ដូនពេញ រាជធានី ភ្នំពេញ។</h3></span>
                                    </div>
                                   
                                     
                                </div>

                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 ">
                                   
                                    <div>
                                        <p>ដើម្បីជម្រុញការកៀរគរធនធានសម្រាប់ការអភិវឌ្ឍជំនាញដែលជំរុញដោយតំរូវការនិងបង្កើតកំលាំងពលកម្មជំនាញដែលមានតម្លៃនិងតំរូវការដោយទីផ្សារ ។</p>
                                    </div>
                                    <div class="text-right">
                                        <button type="button" class="btn btn-primary btn-sm">ចូលរួម</button> 
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div> --}}

                    @if( count($data) > 0 )
                    @foreach($data as $row)
                    <div class="row ">
                        <div class="container-fluid">
                            <div class="row news-items ">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12  ">
                                        {{-- <img src="{{ asset('public/frontend/camcyber/news/3.jpg') }}" alt=""> --}}
                                        <img src="{{ asset( $row->image ) }}" alt="">
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                    
                                    <h3>{{$row->title}} </h3>
                                    <div class="event-listing" style="border-top: 1px solid #f4f6f8;">
                                    <i class="icon-calendar-3  "></i><span>&nbsp;  {{$row->date}}</h3></span>
                                    </div>
                                    <div class="event-listing">
                                    <i class="icon-clock  "></i>&nbsp; <span>{{$row->time}}</h3></span>
                                    </div>
                                   
                                    <div class="event-listing">
                                        <i class="icon-placeholder-3  "></i>&nbsp;  <span>{!!$row->location!!}</h3></span>
                                    </div>
                                   
                                     
                                </div>

                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 ">
                                   
                                    <div>
                                        <p>{{$row->description}}</p>
                                    </div>
                                    <div class="text-right">
                                        <button type="button" class="btn btn-primary btn-sm">ចូលរួម</button> 
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                       @endforeach
                    @else
                            <div class="col-12">
                                <div class="alert alert-warning" role="alert">
                                    គ្មាន​ទិន្នន័យ
                                </div>
                            </div>
                    @endif
                    
                </div>
            </div>
        </div>

    </div>

@endsection