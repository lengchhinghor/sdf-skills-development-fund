@extends('frontend.layout.master') @section('title', 'Welcome to SDF') @section('home', '') @section('active-home', 'active') @section('content') @include('frontend.layout.banner') @section ('appbottomjs')
<script type="text/javascript">
    $(document).ready(function() {
        $("#show-image-up").click(function(event) {
            submit(event);
        })
    })

    function submit(id) {

        $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET
            url: "{{ route('get-application',  ['locale'=>$locale]) }}?id=" + id,
            //url: '{{$locale}}/get-ceramic-color-size/'+id,
            success: function(res) {
                console.log(res);
                var APP_URL = "{{ url('/') }}/";

                var html = "";
                //html = '<div class="col-md-5"> <div class="tab-content product-details-large" id="myTabContent"> <div class="tab-pane fade show active" id="single-slide1" role="tabpanel" aria-labelledby="single-slide-tab-1"> <div class="single-product-img img-full">  </div></div></div><div class="single-product-menu"> <div class="nav single-slide-menu owl-carousel" role="tablist"><div class="single-tab-menu img-full"> <a class="active" data-toggle="tab" id="single-slide-tab-1" href="#single-slide1"><img src="'+APP_URL+res.data.image+'" alt=""></a></div></div></div></div><div class="col-md-7"><div class="modal-product-info"><h1>'+res.data.name+'</h1><div class="cart-description"><table class="hover"><tbody><tr><td>PRODUCT CODE</td><td>'+res.data.product_code+'</td></tr><tr><td>SIZE</td><td>'+res.data.size+'</td></tr><tr><td>SERIES</td><td>'+res.data.series+'</td></tr><tr><td>COLOR</td><td>'+res.data.color+'</td></tr><tr><td>TECHNOLOGY</td><td>'+res.data.technology+'</td></tr><tr><td>SUREFACE LOOK</td><td>'+res.data.structure+'</td></tr></tbody></table></div> </div></div>'
                html = '<div class="process-img mt-3 text-center"> <img src="' + APP_URL + res.data.image + '" alt="" class="" style="width:150px;"></div><p> ' + res.data.content + ' </p>'
                $("#data-show").html(html);

                // Show title
                //  $("#exampleModalLabel").
                $("#exampleModalLabel").html(res.data.title)
            },
            error: function() {
                console.log(res);
            }
        });
    }
</script>

@endsection

<section class="introduction">
    <div class="section-space80 bg-white ">
        <div class="container ">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="section-title mb20 text-center">
                        <h1>{{__('general.vision')}}</h1>
                    </div>
                    <div class="section-title mb60 text-center">
                        <p>{{__('general.vision-text')}}</p>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 d-none d-sm-block">
                    <div class="section-about-video" style="margin: 10px;">
                        <div class="about-img"><img src="{{ asset('public/frontend/camcyber/vision.jpg') }}" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="section-space120 pinside60 " style="background: url('https://media.flaticon.com/dist/assets/ba2ffe8e6369a6edf68369a929a6cbc7x.svg') rgb(0, 108, 184);">
    <div class="container bg-white-priority">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="mt-5 section-title text-center mb30">
                    <h1>{{__('general.priority-sector')}}</h1>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach ($priority_sector as $row)
                <div class="col-xl-2 col-lg-4 col-md-4 col-sm-4 col-6 mb-4">
                    <div class="text-center mb10">
                        <div class="loan-products-icon"><img style="max-width:100px" src="{{ asset ($row->image) }}" /></div>
                        <div class="loan-products-content">
                            <h3>{!!$row->title!!}</h3>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

<div class="section-space80 bg-white process-section">
    <div class="container">
        <div class="row">
            <div class="offset-xl-2 col-xl-8 offset-lg-2 col-lg-8 col-md-12 col-sm-12 col-12">
                <div class="section-title text-center">
                    <h1>{{__('general.application-procedure')}}</h1>
                    {{-- <p>{{__('general.application-procedure-content')}}</p> --}}
                </div>
            </div>
            <div class="row d-flex justify-content-center">
                @foreach( $application as $row )
                <div class="col-xs-12 col d-none d-xl-block">
                    <div class="application-bg number-block mb30 ">
                        @if(!$loop->last)
                        <div class="step-application d-none d-xl-block">
                            <div class=""><i class="fa fa-arrow-right" aria-hidden="true"></i></div>
                        </div>
                        @endif
                        <div class="process-img mt-3" onclick="submit({{$row->id}})">
                            <a href="" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"><img src="{{ asset ($row->image) }}" alt="" class=""></a>
                        </div>
                        <div class="pinside20">
                            <h3>{{ $row->title }}</h3>
                        </div>
                    </div>
                </div>
                <div class="col-6 d-xl-none mobile-display">
                    <div class="application-bg number-block">
                        @if(!$loop->last)
                        <div class="step-application d-none d-lg-block d-xl-none">
                            <div class=""><i class="fa fa-arrow-right" aria-hidden="true"></i></div>
                        </div>
                        @endif
                        <div class="process-img mt-3" onclick="submit({{$row->id}})">
                            <a href="" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"><img src="{{ asset ($row->image) }}" alt="" class=""></a>
                        </div>
                        <div class="pinside20">
                            <h3>{{ $row->title }}</h3>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

<div class="section-space80">
    <div class="container">
        <div class="row">
            <div class="offset-xl-2 col-xl-8 offset-md-2 col-md-8 offset-md-2 col-md-8 col-sm-12 col-12">
                <div class="mb30 text-center section-title">
                    <h1>{{__('general.news')}}</h1>
                </div>
            </div>
        </div>
        <div class="row d-flex justify-content-center">
            @foreach($relatedPosts as $row)
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="card">
                    <a href="{{route('news-detail', [ 'locale'=>$locale, 'id'=>$row->id])}}">
                <img src="{{ asset ($row->image) }}" class="card-img-top img-fluid" alt="...">
                </a>
                    <div class="card-body">
                        <b class="card-text" style="color:black;">{{ mb_substr($row->title, 0, 70, "utf-8") }}...<span style="color:#006cb9;"><a href="{{route('news-detail', [ 'locale'=>$locale, 'id'=>$row->id])}}">{{__('general.read-more')}}</a></span><a href=" {{route('news-detail', [ 'locale'=>$locale, 'id'=>$row->id])}} "><span style="margin-left: 10px;"></span></a></b>
                    </div>
                    <div class="p-2 bottom-date">
                        <h5 class="card-title" style="color:black;">{{$row->date}}</h5>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

    </div>
</div>
</div>


<div class="section-space40 bg-white">
    <div class="container">
        <div class="row">
            <!--  <div class="col-xs-12"> -->
            <div class="owl-carousel owl-theme partner-owl">
                @foreach($partner as $row)
                <div class="item">
                    <a target="_blank" href="{{ asset ($row->url) }}"> <img src="{{ asset ($row->image) }}" class="img-center " alt=""></a>
                </div>
                @endforeach
            </div>
            <!--  </div> -->
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h3 class="modal-title" id="exampleModalLabel"> </h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div id="data-show">

                </div>

            </div>
        </div>
    </div>
</div>

@endsection

<style>
    .modal-header {
        background-color: #006cb9;
    }
    
    .loan-products-content h3 {
        font-size: 16px;
        color: #006cb8;
    }
    
    .process-img img {
        width: 100%;
        max-width: 50%;
    }
    
    .card-body {
        border-top: 4px solid #006cb9;
        padding: 10px;
    }
    
    .bottom-date {
        background-color: #fbfbfb;
    }
    
    .application-bg {
        padding-top: 5px;
    }
    
    .pinside20 {
        padding-bottom: 10px !important;
        padding-top: 10px !important;
    }
    
    .step-application {
        position: absolute;
        right: 0;
        top: 55px;
        font-size: 50px;
        color: #006cb885;
    }
    
    .modal-content {
        border: 0px solid rgba(0, 0, 0, .2) !important;
    }
    
    .close {
        color: #fff !important;
    }
    
    .modal-title {
        color: white;
    }
    
    @media (max-width:600px) {
        .process-img img {
            width: 100%;
            max-width: 60%;
        }
        .mobile-display{
            height: 220px;
        }

    }
</style>