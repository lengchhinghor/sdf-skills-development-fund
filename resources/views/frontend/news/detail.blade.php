@extends('frontend.layout.master') 
@section('title', 'Welcome to SDF') 
@section('active-news', 'active') 
@section('content')

@if($banner)
<div class="page-header" style="background-image: url({{ asset ($banner->image)}});">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-breadcrumb">
                    <ol class="breadcrumb">
                        <li><a href="#">{{__('general.news-and-events')}}</a></li>
                        <li class="active">{{__('general.news')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<div class="container">

    <div class="wrapper-content bg-white ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                    <div class="about-section news-detail-cnt ">

                        {{-- ============================== News Detail ================================ --}}
                        <div class="row ">

                            <div class="title col-12">
                                <h3>{!! $data->title ?? '' !!}</h3>
                            </div>
                            <div class="features col-12">
                                <span class="date"><i class="fa fa-calendar" style=" color: #006cb9;" aria-hidden="true"></i> {!! $data->date !!}</span>
                            </div>

                            <div class="col-12">
                                <div class="mb-3">
                                    <img src="{{ asset ($data->image) }}" class="img-fluid" alt="Responsive image">
                                    {{-- <img src="{{ asset ($data->image) }}" class="rounded mx-auto d-block" alt="..."> --}}
                                </div>
                                {!! $data->content !!}
                            </div>

                        </div>

                        {{-- ============================== Images ================================ --}}
                        <div class="row">

                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 sidebar-cnt">
                    <div class="col-10">
                        <div class="sidebar-title">
                            <h3>{{__('general.news-and-events')}}</h3>
                        </div>
                        <div class="sidebar-listing">
                            <ul class="list-group">
                                <li class="list-group-item"> <a href=" {{ route('news', $locale) }}">{{__('general.news')}}</a> </li>
                                <li class="list-group-item"><a href="{{ route('announcement', $locale) }}">{{__('general.announcement')}}</a> </li>
                                <li class="list-group-item"><a href="{{ route('event', $locale) }}">{{__('general.events')}}</a> </li>
                                <li class="list-group-item"><a href="{{ route('career', $locale) }}">{{__('general.careers')}}</a> </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>

@endsection