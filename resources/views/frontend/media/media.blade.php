@extends('frontend.layout.master') 
@section('title', 'Welcome to SDF') 
@section('active-news', 'active')


@section('content')

        @if($banner)
    <div class="page-header" style="background-image: url({{ asset ($banner->image)}});">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li><a href="#">{{__('general.news-and-medias')}}</a></li>
                            <li class="active">{{__('general.Media')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
        @endif

    <div class="wrapper-content bg-white container">

        <div class="row">
            <div class="col-xl-12">
                <div class="about-section news-detail-cnt ">

                    {{-- ============================== Page Header ================================ --}}
                    <div class="row">
                        <div class="col-xl-12 director-title-top mb-4">
                            <div class="title">
                                <h3 class="">{{__('general.Media')}}</h3>
                            </div>
                        </div>
                    </div>

                    @if( count($data) > 0 )
                    @foreach($data as $row)
                    <div class="row ">
                        <div class="container-fluid">
                            <div class="row news-items ">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12  ">
                                     {!! $row->content !!}
                                     {{-- <b style="color:black"><h3>{!!$row->title!!}</h3></b> --}}
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12  ">
                                <b style="color:black">{!!$row->title!!}</b>
                              
                               </div>
                            </div>
                        </div>
                    </div>
                       @endforeach
                    @else
                            <div class="col-12">
                                <div class="alert alert-warning" role="alert">
                                    គ្មាន​ទិន្នន័យ
                                </div>
                            </div>
                    @endif
                    
                </div>
            </div>
        </div>

    </div>

@endsection