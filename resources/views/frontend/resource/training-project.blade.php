 @extends('frontend.layout.master')
 @section('title','Welcome to SDF')
 @section('active-resources', 'active')
 @section('content')

 @foreach($banner)
 <div class="page-header" style="background-image: url({{ asset ($banner->image)}});">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">Scheme</li>
                        </ol>
                    </div>
                </div>
              
            </div>
        </div>
    </div>
    <div class=" ">
        <!-- content start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="wrapper-content bg-white ">
                        <div class="about-section pinside40">
                            <div class="row">
                            
                                <div class="col-xl-12">
                                    <div class="mb20">
                                        <h3 class="" style="border-bottom:2px solid #006cb9;">Training Project Proposal Template</h3>
                                        <p class=" mb30">This training scheme is the standard and most demanded form under SDF. It supports and co-finances the skills development of current or future employees. Employers are teaming up with private or public training providers to develop training programs for their current staff or pre-employment training for which they plan to hire after completion of training. Such training programs are not limited in content or form; the importance is the value addition of a better-skilled and productive workforce to employers. Therefore, the principle is that training, which employers are willing to cost-share (financially or in-kind) and to which they are sending their staff must be relevant training. Consequently, technical and soft-skills training, or a mix of both, are eligible.
In order to apply to the SDF, employers and schools are submitting joint training proposals which include Key Performance Indicators to measure the performance of the training project. If selected, SDF will co-finance the training project and the employer and/or training provider will cost-share (financially or in-kind) the overall training cost.
The current priority sectors are construction, manufacturing, ICT, tourism, electrics, and electronics but also other high-value added sectors can be considered. if a training proposal is successfully approved, SDF finance will be disbursed in tranches in order to provide training providers with necessary resources upfront, to ensure and quality assures training provision, to allow for assessment of gained competencies, and last but certainly not least to incentivize high employment rates and increased salaries.
                                        </p>
                                    </div>
                                </div>
        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
 @endsection

