   <div class="slider" id="slider">
        <!-- slider -->
        @php ($i = 1)
        @foreach ($slide as $row)
        <div class="slider-img"><img src="{{ asset ($row->image) }}" alt="No DATA" class="">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="slider-captions">
                          <!--   <h1 class="slider-title">{{ $row->title}} </h1> -->
                          <!--   <a href="#" class="btn btn-default">Click for Apply</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
         @endforeach
    </div>
