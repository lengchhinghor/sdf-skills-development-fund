@if ($paginator->hasPages())
    
        
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                
            @else
                <!-- <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo;</a></li> -->
                <a id="data" href="{{ $paginator->previousPageUrl() }}"><span style="float: left;"><i class="fas fa-chevron-circle-left color1"></i></span></a>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <!-- <li class="disabled"><span>{{ $element }}</span></li> -->
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    <!-- @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="active"><span>{{ $page }}</span></li>
                        @else
                            <li><a href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach -->
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
               <!--  <li><a href="{{ $paginator->nextPageUrl() }}" rel="next">&raquo;</a></li> -->
               <a id="data" href="{{ $paginator->nextPageUrl() }} "><span><i class="fas fa-chevron-circle-right color1"></i></span></a>
            @else
               <!--  <li class="disabled"><span>&raquo;</span></li> -->
            @endif
       
   
@endif
