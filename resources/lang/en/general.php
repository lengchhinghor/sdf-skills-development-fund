<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    //============ Manu
    'home' => 'Home',

    'about-us' => 'About Us',
    'history-of-project' =>'History of project',
    'director-speech' => 'Director’s Speech', 
    'sdf-organizational-structure' => 'SDF Organizational Structure',
    'structural-Functions' => 'Structural Functions',
    
    'scheme' =>'Scheme',
    'training-large' => 'Training for Large Firms/Associations', 
    'training-for-smes' => 'Training for SMEs/Associations', 
    'training-of-trainer' => 'Training of Trainers',

    'resources' =>'Resources',
    'selection-criteria' => 'Selection Criteria',   
    'guidelines-for-proposal-submission' => 'Guidelines for Proposal Submission',
    'faqs'=> 'FAQ', 
    'training-project-proposal' => 'Training Project Proposal Template', 

    
    'news-and-events'=>'News And Events', 
    'news' => 'News',
    'careers' => 'Careers',
    'events' => 'Events',
    'announcement' => 'Announcement',

    'contact-us' => 'Contact Us', 

    // <!========about-section======>
    'what-skills-development-fund' =>'What is a Skills Development Fund?',
    'welcome-to-sdf'=>'Welcome to Skills Development Fund website !!!',
    'director-speech-content' =>'Cambodia’s economy has experienced remarkable growth over the past two decades, on average around 7.7% per annum. In order to sustain high and resilient growth, especially to achieve Cambodia Vision of becoming an upper middle-income country by 2030, Royal Government has been implementing Industrial Development Policy 2015-2025 to further improve Cambodia’s economic competitiveness and diversified sources of growth, and more importantly to transform the economy from a labor-intensive to a skill-based economy.

    In this regard, the RGC has laid out a number of measures to improve the provision of skilled workforce through various policies, strategies, programs and projects implemented by government ministries and institutions, particularly by Ministry of Labor and Vocational Training and Ministry of Education, Youth and Sports as well as joint programs and projects implemented in partnership with development partners and non-government organizations.',
    'read-more' =>'...Read more',
    'priority-sector' =>'Priority Sector',
    'priority-contents'=>'With the rapid growth of technology, the Royal Government of Cambodia ...',
    'application-procedure'=>'Application process',
    'application-procedure-content' =>'You can apply your training proposal for SDF funding online through the link (…).',
    'click-apply' => 'Click for Apply',
    'vision' => 'Vision',
    'vision-text' => 'To be the most trusted financing platform for sustainable and demand-driven skill development.',

    // ==============================ABOUT US==============  
    'history-content' =>'Tens of thousands of Cambodia’s young population enter the labor force each year. Labor force participation among the working age population (15–64 years) increased from 7.7 million in 2009 to 8.3 million in 2015. While the country is endowed with an ample supply of labor, the labor force is still characterized by very low levels of education and skill. The average educational attainment of the current workforce is primary school completion (grade 6) or below—a major barrier to industrial diversification efforts and moving up the value chain. The 2015 Cambodia Socio-Economic Survey found that nearly half (46%) of the labor force have either not completed primary education or never attended school. Less than 7% of workers completed high school (grade 12). There is a pressing need to respond to the growing demand for an adaptable workforce with professional skills and proper workplace behavior. In response, the government has taken steps to reform and strengthen the education and training system, including an enhanced role of the private sector in skills development and providing incentives to enterprises to train their own workforce.

    The pilot Skills Development Fund (SDF) is an industry driven workforce development program. The general notion is to build confidence in industry partnership and to engage more strongly with the private sector and their training needs. Firms willing but at times hesitant to further develop their workforce may be supported by the SDF to further invest in workforce development. Motivated private workforce development initiatives and intentions shall be mainly supported by the SDF (which is in turn consisting of a motivated, skilled, and client oriented team).
    
    ',
    'integrity' =>'Integrity',
    'accountability' =>'Accountability',
    'continuous-improvement' =>'Continuous Improvement',   

     //=======================Director Speech================
     'director-speech-text1' => 'Cambodia’s economy has experienced remarkable growth over the past two decades, on average around 7.7% per annum. In order to sustain high and resilient growth, especially to achieve Cambodia Vision of becoming an upper middle-income country by 2030, Royal Government has been implementing Industrial Development Policy 2015-2025 to further improve Cambodia’s economic competitiveness and diversified sources of growth, and more importantly to transform the economy from a labor-intensive to a skill-based economy.',
     'director-speech-text2' => 'In this regard, the RGC has laid out a number of measures to improve the provision of skilled workforce through various policies, strategies, programs and projects implemented by government ministries and institutions, particularly by Ministry of Labor and Vocational Training and Ministry of Education, Youth and Sports as well as joint programs and projects implemented in partnership with development partners and non-government organizations.',
     'director-speech-text4' => 'To complement the efforts of other government relevant ministries and institutions to accelerate the building of highly skilled workforce to meet the demand of private sector, Ministry of Economy and Finance has initiated a training fund called Skills Development Fund (SDF). SDF is innovative partnership between public-private sectors in skill development, which is recognized as an effective approach to better equip workforce with the right skills and meet the demand of private sector. In fact, funds for training such as SDF have been commonly used by many governments around the globe to address skills needs by the private sector for the following three reasons: (1) private sector has a platform, where they could seek co-financing from the government to fund training projects to upgrade their workers, (2)  government could minimize risks of labor supply mismatch to the need of the job market, and (3) training fund such as SDF would encourage training providers and industry to work closer in jointly developing and implementing the training programs, and allow trainers at school to expose to latest equipment, technologies, and practices at industry which would foster school-industry linkages.',
     'director-speech-text5' => 'Through SDF, the RGC is taking a serious and innovative approach to the development of Cambodia’s workforce. SDF project is indeed supported by reputable development partners, such as the Asian Development Bank (ADB) and the Agence Française de Développement (AFD). The SDF is co-financing both upskilling workers who are currently working and providing training to new workforce. The fund also targets training of trainers (ToT) who currently work in schools or companies so they could upgrade their expertise. SDF welcome proposals for training in both soft skills and technical skills initiated jointly by private sectors, schools and training providers and target key industries such as manufacturing, electronics, construction, ICT, tourism, etc.',
     'director-speech-text6' => 'Signature',

     //===========Scheme==============
     'training-large-text1' =>'This training scheme is the standard and most demanded form under SDF. It supports and co-finances the skills development of current or future employees. Employers are teaming up with private or public training providers to develop training programs for their current staff or pre-employment training for which they plan to hire after completion of training. Such training programs are not limited in content or form; the importance is the value addition of a better-skilled and productive workforce to employers. Therefore, the principle is that training, which employers are willing to cost-share (financially or in-kind) and to which they are sending their staff must be relevant training. Consequently, technical and soft-skills training, or a mix of both, are eligible.',
     'training-large-text2' =>'In order to apply to the SDF, employers and schools are submitting joint training proposals which include Key Performance Indicators to measure the performance of the training project. If selected, SDF will co-finance the training project and the employer and/or training provider will cost-share (financially or in-kind) the overall training cost.',
     'training-large-text3' =>'The current priority sectors are construction, manufacturing, ICT, tourism, electrics, and electronics but also other high-value added sectors can be considered. if a training proposal is successfully approved, SDF finance will be disbursed in tranches in order to provide training providers with necessary resources upfront, to ensure and quality assures training provision, to allow for assessment of gained competencies, and last but certainly not least to incentivize high employment rates and increased salaries.',

     'training-sme-text1' => 'This training scheme addresses the shortages in SMEs’ skilled workforce and/or entrepreneurial/management skills of SME owners/Startups. The focus of this scheme is to improve entrepreneurial skills and mindsets of SME owners/Startups and/or their employees. Participants will be equipped with new tools and techniques to facilitate (potential) entrepreneurs to start businesses as well as to facilitate the growth of existing SMEs.',
     'training-sme-text2' => 'Training courses will allow the participants to acquire skills to: (i) Initiate, plan and implement entrepreneurship development activities for foundation of small enterprises; (ii) Effectively counsel the entrepreneurs in various areas such as: market identification, product development, establishment and management of new enterprises as well as corporate governance, growth and diversification of existing enterprises.',
     'training-sme-text3' => 'Practical entrepreneurship education will help participants acquire relevant attitude, skills, and knowledge to breed, accelerate and incubate start-up businesses, or to improve the performance and effectiveness of existing SMEs.',

     'training-of-trainer-text1' => 'The Training of Trainers program is designed to support companies and training providers to create a cadre of competent trainers who in turn train trainers, particularly in areas/skills of severe shortage or of high demand in the future. Training of Trainers (ToT) is key in education development since this is the multipliers when it comes to who can train a skilled workforce.',
     'training-of-trainer-text2' => 'ToT consists of didactic and technical components which help Trainers to be well-equipped with appropriate and state of art teaching and learning methods and necessary technical competencies. Teaching and learning competencies are the abilities of trainers to explain adult learning principles and how training can be delivered effectively to adults, conducting training needs analysis, designing competency-based training programs and effective training delivery. Moreover, ToTs are also capacitated to improve attitude, motivation, and work-ethics of their future trainees.',

     //=====Resource===================
     'scope-and-pass' => '1. Scope and Pass/Fail Criteria (for shortlisting)',
    
     //  guideline
     '5step-content' => '5 Steps to apply for funding from Skills Development Fund (SDF). As first step, TVET schools/training providers discuss with companies about needs for skills training and once training needs are identified (either pre-employment training or up-skilling of workforce), submit a joint-training proposal to Project Management Unit (PMU) of SDF, Ministry of Economy and Finance. As second step, the PMU will validate proposals by reviewing submitted documents and making site visits. Then, the PMU will shortlist the proposal, using information from validation stage and Pass/Fail criteria. The PMU will submit the shortlisted proposals to Selection Committee for evaluation based on standardized scoring sheet. As third step, the Selection Committee will submit a list of recommended proposals to Executing Agency (EA) for final approval. As fourth step, PMU will disclose the list of successful proposals to the public. And, as fifth step, all successful proposal will be invited for signing contracts with PMU and the implementation of the training project can start.',
     'ilustration-of-proposal-submission' => 'Illustration of Proposal Submission Procedures',
     'acceptance-and-deadline' =>  'Acceptance and Deadline Of Proposals Submission',
     'acceptance-and-deadline-content' => 'Acceptance of proposal is from May 16th until July 31st, 2019. Please submit your proposal electronically to SDF PMU (info@sdfcambodia.org). Applicants may also submit proposals ad hoc anytime, which will be reviewed on a case by case basis. The template of the proposal is enclosed with this guideline.',
      
     'about-sdf-v2' => '',
     'about-sdf-v3' => 'The fund has been in existence for three years from 2018-2021 and is being managed by a majority of the parties. The Ministry of Planning and Finance (MEF), through a professional contractor, has been working on the project. Developing relevant departments such as professional and vocational training, youth education, and Sports Medicine as well as other professional services.',
     'introduction-about-the-sdf-pilot-project' =>'Introduction: About the SDF - Pilot Project ',
     'introduction-about-the-sdf-pilot-project-content' =>'SDF is a newly-adopted policy instrument envisioned to become the most trusted financing platform for sustainable and demand-driven skill development. It aims to enhance allocative efficiency of public funding and stimulate pragmatic public private partnership projects in bridging the skill gaps in an effort to attracting higher-value-added investments and generating decent jobs to realize the Government’s vision of becoming a higher middle-income country by 2030. The fund is for 3 years from 2018-2021 and is managed by Project Management Unit at Ministry of Economy and Finance (MEF) in close collaboration with development partners, relevant ministries such as Ministry of Labor and Vocational Training, Ministry of Education, Youth and Sports, etc. as well as Business Associations.',
     'priorities-and-target-group' =>'Priorities and Target Group',
     'priorities-and-target-group-content' =>'SDF will focus on 5 priority sectors: Manufacturing, Construction, ICT, Electronics, and Tourism. In addition, the fund can accommodate other sectors that have high demand for skilled workforce and are emerging and contributing to the diversification of Cambodia’s economy (for detailed information on selection criteria, see Selection Criteria for PPP Training Projects enclosed with this guideline) The fund targets both upskilling and pre-employment training projects to improve soft skills and technical skills of Cambodia’s workforce. In other words, the fund could be used to upskill workers who are currently employed in foreign and local companies in Cambodia or to provide training to new workforce so they are better prepared for their future jobs. Priority will be given to training projects that are jointly developed and delivered by TVET schools/training providers and companies, with substantial practice at industry/workplace. Priority will also be given to training programs that have substantial in-kind and financial costsharing by schools and companies.',
     'who-can-apply' =>'Who Can Apply',
     'who-can-apply-content1' =>'SDF Pilot is open to companies, enterprises and factories in Cambodia or plan to invest in Cambodia, which are registered with the Royal Government of Cambodia. All companies and schools in the priority sectors indicated above are eligible for this fund regardless of their size or ownership. Priorities for this round of call for proposal will be given to small and medium size enterprises. They can submit proposals by themselves or through business associations.',
     'who-can-apply-content2' =>'Priorities will be given to local schools/training providers. They can be public or private schools/training providers or NGOs. The submission of proposals for this fund requires the companies and schools to have a joint collaboration with each other. Companies and schools receiving this fund are required to issue certificates for participants that are signed jointly by company and school or other certificates issued by ministries as stipulated in the CQF of Cambodia.',
     'who-can-apply-content3'=> 'A company and school can submit MORE THAN one proposal at a time.',
     'type-of-skills-training-and-possible'=>'Type of Skills Training and Possible Duration of Projects',
     'type-of-skills-training-and-possible-content'=> 'The training projects eligible for this fund are for both technical skills and soft skills training and the duration could be from 2 weeks to 18 months.',
     'unit-cost-of-the-training' =>'Unit Cost of the Training',
     'unit-cost-of-the-training-content' =>'The proposal has to indicate the unit cost per student, which shall not exceed 8,000,000 Riel/student/program (US$2,000/student/program). Minimum size of training course is 12 participants.',
     'eligible-expenses-and-ineligible-expenses' =>'Eligible Expenses and Ineligible Expenses',
     'eligible-expenses-and-ineligible-expenses-content' =>'This fund can only be used to cover operational costs of the training such as salaries and allowances for trainers, allowances for trainees, training materials, utilities and other administrative costs. This fund cannot be used for capital investment such as constructing buildings or purchasing training equipment.',
     'language-of-the-proposal'=>'Language of the Proposal',
     'language-of-the-proposal-content'=>'Proposals can use either Khmer or English.',
     'reporting-requirements' => 'Reporting Requirements',
     'reporting-requirements-content1' => 'Successful proposals will be required to comply with monitoring requirements, which include quarterly narrative and financial reports. Templates of reports will be provided by SDF PMU. They will also be required to provide information about project milestones (e.g. announcements of the forthcoming events, news about completed events with photos, etc.) according to individual communication plan that will be developed at the contracting stage.',
     'reporting-requirements-content2' => 'At the end of the project, the training projects are evaluated based on the following Key Performance Indicators (KPI)',
     'disbursement-plan' => 'Disbursement Plan',
     'disbursement-plan-content1' => '1. 10% upfront upon signing the contract',
     'disbursement-plan-content2' => '2.40% after submission of required training enrollments',
     'disbursement-plan-content3' => '3.30% is made after completing the training midway under the condition that drop-out rate is below 20% and submission of a) training progress report and b) list of training materials, with copies of the training materials in hard copies or soft copies;',
     'disbursement-plan-content4' => '4.20% after made after achieving the three KPIs (drop-out rate, employment rate, salary rate) indicated in the proposal, which will be verified by SDF.',
     'note' => 'Note :',
     'note-content' => 'The disbursement plan is subject to change based on nature of individual training projects.',

     'recipients-of-financing' => 'Recipients of Financing',
     'recipients-of-financing-content' => 'Disbursements will be only made to juridical persons, not individuals. Disbursements will be made to both schools/training providers and companies according to the costs agreed in the training proposal.',
     'contact-of-further-inquiries' => 'Contact of Further Inquiries',
     'contact-of-further-inquiries-content' => 'For support please contact SDF helpdesk through one of these channels:',
     'contact-of-further-inquiries-phone' => '• Phone: 023 901 583',
     'contact-of-further-inquiries-telegram' => '• Telegram: 012 635 294',
     'contact-of-further-inquiries-email' => '• Email: cambodia.sdf@gmail.com',
     'download-the-whole-page' => 'Download the whole page as ',

     //=====Selection Criteria=======

     'scope-and-pass-fail' => '1. Scope and Pass/Fail Criteria (for shortlisting)',
     'priority-sector' => ' Priority sectors',
     'manufacturing' => ' Manufacturing, Construction, ICT, Electronics, Tourism and other high-demand skills',
     'target-beneficiaries' => 'b)	Target beneficiaries',
     'formally-employed' => '',
     'total-training-length' => '',

    //======Sent message=====
    'name'=>'Name',
    'email'=>'Email',
    'phone'=>'Phone',
    'company'=>'Company',
    'message'=>'Message',
    'submit' => 'SUBMIT',
    'sent-us-message' => 'Sent Us Message',
    'Proposal Submission Procedures' => 'នីតិវិធីដាក់ពាក្យស្នើសុំ',

    //======Address==========
    'description-address' => 'To stimulate resource mobilization for demand-driven skill development and generate skilled labors that are valued and needed by the market..',
    'address' => 'St. 92, Rukha Vithei Daun Penh, Sangkat Wat Phnom, Khan Daun Penh, Phnom Penh, Cambodia',
    'address-phone' => '+(855) 23 901 583/12 635 294',
    'ministry-of-economic-and-finance'=> 'Ministry of Economy and Finance (MEF)',
    'copyright' => '© Copyright 2020 | Skills Development Fund (SDF)',
    'email-address-contact' => 'Address: St.92, Sangkat Wat Phnom, Khan Daun Penh, Phnom Penh, Cambodia',
    'phone-number2' => '+(855)23 901 583/12 635 294',
    'email-contact1' => 'info@sdfcambodia.org',
    'email-contact2' => '• Email:info@sdfcambodia.org',
    'phone-number2' => '+(855)23 901 583/12 635 294',
    //=========seletion========
    'a' => 'A',
    'KPI' => 'KPI',
    'Large-Firms' => 'Large Firms',
    'SMEs' => 'SMEs  ',
    'Drop-out-rate' => 'Drop-out rate',
   
    'b' => 'B',
    'Employment-rate-(for Pre-employment only)' => 'Employment rate (for Pre-employment only)',
    '≥ 75%-of-graduated-students-are-formally-employed-in-any-companies-within-3-months-after-training-completion-in-relevant-occupation' => '≥ 75% of graduated students are formally employed in any companies within 3 months after training completion in relevant occupation',
    'c' => 'C',
    'Salary-of-trainees-after-3-months-of-completion' => 'Salary of trainees after 3 months of completion ',
    'up-skilling' => '-up-skilling',
    'up-skilling1' => '≥ 110% of minimum wage of garment and footwear sector (only basic salary)',
    'pre-employment1' => '-pre-employment',
    '≥ 10%-of-current-salary' => '≥ 10% of current salary',
    '≥ 10%-of-current-salary1' => '≥110% of the reference salary $150 (only basic salary) ',
    'Drop-out-rate-in-ToT-≤ 10%-of-enrolled-trainers' => 'Drop-out rate in ToT ≤ 10% of enrolled trainers',
    'Number-of-trainers-completed-ToT-successfully-≥-90%-of-enrolled-trainers' => 'Number of trainers completed ToT successfully ≥ 90% of enrolled trainers',
    'Number-of-trainees-that-successful-trainer-in-ToT-provided-training-within-5-months-after-ToT-completion-≥-25-trainees' => 'Number of trainees that successful trainer in ToT provided training within 5 months after ToT completion ≥ 25 trainees',
    'pre-employment' => 'pre-employment',
    '≥ 110%-of-minimum-wage-of-garment-and-footwear-sector-(only basic salary)' => '≥ 110% of minimum wage of garment and footwear sector (only basic salary)',
    '≥110%-of-the-reference-salary-$150-(only basic salary)' => '≥110% of the reference salary $150 (only basic salary)',
    '≤ 25%-of-enrolled-students' => '≤20% of enrolled students',
    'Media' => 'Media',
    'news-and-medias'=>'News and Medias',
    
];
