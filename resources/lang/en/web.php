<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    
    
    //=============  HomePage
   
    'sitemap' => 'Sitemap',
    'about' => 'About',
    'career' => 'Career',
    'contact' => 'Contact',
    'lastest-news' => 'Lastest News',
    'our-bussiness' => 'Our Bussiness',
    'more' => 'More',
    'our-international-corporations' => 'Our International Corporations',
    'location' => 'Phnom Penh Special Economic Zone National Road No.4, 12509 Khan Posenchey, Phnom Penh, Cambodia.',
    'download-company-presentation' => 'Download company presentation',
    'public-holidays' => 'Public Holidays',
    'hotline' => 'Hotline Number: 015 668 544 - Electricity Hotline 086 581 777 - Security Hotline',

    //=================== Investor Relation
    'investor-relation' => 'Investor Relation',
    'welcome-to-investor-relation' => 'Welcome to Investor Relations',
    'corporate-information' => 'Corporate information',
    'business-overview' => 'Business Overview',
    'company-structure' => 'Company Structure',
    'board-of-director' => 'Board of Director',
    'management-team' => 'Management Team',
    
    'financial-information' => 'Financial information',
    'financial-highlights' => 'Financial Highlights',
    'financial-statements' => 'Financial Statements',
    'stock-information' => 'Stock Information',

    'stock-quote' => 'Stock Quote',
    'historical-price' => 'Historical price',
    'shareholder-information' => 'Shareholder information',
    'fact-sheet' => 'Fact Sheet',
    'major-shareholders' => 'Major Shareholders',
    'dividend-information' => 'Dividend Information',
    'ir-calendar' => 'IR Calendar',
    'shareholder-meetings' => 'Shareholders meetings',
    'corporate-governance' => 'Corporate Governance',
    'news-room' => 'News Room',
    'csx-secc-announcement' => 'CSX/SECC Announcemt',
    'press-release' => 'Press Release',
    'press-review' => 'Press Review',
    'publication-and-download' => 'Publication and Download',
    'periodic-report' => 'Periodic Report',
    'financial-statments' => 'Financial Statements',
    'company-snopshot' => 'Company Snapshot',
    'information-inquire' => 'Information Inquire',
    'md-a' => 'MD & A',

    'searching-result'=>'Searching Result',
    'no-search-result-foud'=>'Sorry, there is no content found related to your keyworkd.',
];