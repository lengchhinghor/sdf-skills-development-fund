<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    
    
    //=============  HomePage
    'sitemap' => '网站地图',
    'about' => '关于',
    'career' => '事业',
    'contact' => '联系',
    'lastest-news' => '最新消息',
    'our-bussiness' => '我们的业务',
    'more' => '更多',
    'our-international-corporations' => '我们的国际公司',
    'location' => '金边经济特区柬埔寨金边Khan Posenchey 12509号4号国道。',
    'download-company-presentation' => '下载公司介绍',
    'public-holidays' => '公共假期',
    'hotline' => '热线号码：015 668 544 - 电话热线086 581 777 - 保安热线',

    //=================== Investor Relation
    'investor-relation' => '投资者关系',
    'welcome-to-investor-relation' => '欢迎投资者关系',
    'corporate-information' => '公司信息',
    'business-overview' => '商业概览',
    'company-structure' => '公司结构',
    'board-of-director' => '董事会',
    'management-team' => '管理团队',
    
    'financial-information' => '财务信息',
    'financial-highlights' => '财经摘要',
    'financial-statements' => '财务报表',
    'stock-information' => '股票信息',

    'stock-quote' => '股票报价',
    'historical-price' => '历史价格',
    'shareholder-information' => '股东信息',
    'fact-sheet' => '情况说明书',
    'major-shareholders' => '主要股东',
    'dividend-information' => '股息信息',
    'ir-calendar' => 'IR日历',
    'shareholder-meetings' => '股东大会',
    'corporate-governance' => '公司治理',
    'news-room' => '新闻室',
    'csx-secc-announcement' => 'CSX / SEC公告',
    'press-release' => '新闻稿',
    'press-review' => '新闻评论',
    'publication-and-download' => '出版和下载',
    'periodic-report' => '定期报告',
    'financial-statments' => '财务报表',
    'company-snopshot' => '公司快照',
    'information-inquire' => '信息查询',
    'md-a' => 'MD & A',

    'searching-result'=>'搜索结果',
    'no-search-result-foud'=>'抱歉，没有找到与您的关键字相关的内容。',
];
