<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\Builder;
use App\Model\Content;
use App\Model\History as History;
use App\Model\Organization;
use App\Model\Banner;
class AboutController extends FrontendController
{

    public function directorSpeech($locale="en"){
		$defaultData = $this->defaultData($locale);
		$banner = Banner::select('image','is_published')->find(17);
		// dd($banner);
		$data = Content::select($locale.'_title as title', $locale.'_content_part1 as content_part1', $locale.'_content_part2 as content_part2', 'image')
		->where('slug', 'director-speech ')
		->first();
		return view('frontend.about.director-speech',
			[
				'defaultData'		=>	$defaultData,
				'locale'			=>	$locale,
				'data'				=>	$data,
				'banner'			=>  $banner,
			]);

	}
	
	
	public function history($locale="en"){
		$defaultData = $this->defaultData($locale);
		$banner = Banner::select('image','is_published')->find(17);
		$data = Content::select ( $locale.'_title as title',$locale.'_content_part1 as content_part1', $locale.'_content_part2 as content_part2' )
		->where('slug','history')
		->first();
		return view('frontend.about.history',
			[
				'defaultData'	=>	$defaultData,
				'locale'		=>	$locale,
				'data'			=>	$data,
				'banner' 		=>  $banner,
				
	 	]);
	}
	
	public function organization($locale="en"){
		$banner = Banner::select('image','is_published')->find(17);
    	$defaultData = $this->defaultData($locale);
    	$organizatin=Organization::select('id','image')->get();
    	return view('frontend.about.organization',['defaultData'=>$defaultData,'locale'=>$locale,'organization'=>$organizatin,'banner'=> $banner]);
    }
}
