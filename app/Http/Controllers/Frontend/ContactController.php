<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Http\Request;
use App\Http\Requests;

use Session;
use Validator;
// use Telegram\Bot\Laravel\Facades\Telegram;

use App\Model\Massage as Message;
use App\Model\Banner;
class ContactController extends FrontendController
{

    public function index($locale="en"){
        $defaultData = $this->defaultData($locale);
        $banner = Banner::select('image','is_published')->find(16);
    	return view('frontend.contact-us.contact-us',['defaultData'=>$defaultData,'locale'=>$locale,'banner'=>$banner]);
    }

    public function sendMassage(Request $request){
        
        $now        = date('Y-m-d H:i:s');
        
        $data = array(
            'name'              =>      $request->input('name'),
            'phone'             =>      $request->input('phone'),
            'email'             =>      $request->input('email'),
            'company'           =>      $request->input('company'),
            'message'           =>      $request->input('message'),
            'created_at'        =>      $now
        );
        
        Session::flash('invalidData', $data );
        Validator::make(
            $request->all(), 
            [
                'name' => 'required',
                'phone' => 'required',
                'email' => 'required',
                'message' => 'required',
                'company' => 'required',
            ], 

            [
                
            ])->validate();

        
        $id = Message::insertGetId($data);
        
        $request->session()->flash('msg', 'Your request has been sent! We will respone you soon.' );
        return redirect()->back();
    }
}
