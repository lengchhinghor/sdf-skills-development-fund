<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\Model\History as History;
class HistoryController extends FrontendController
{

    public function index($locale="en"){
    	$defaultData = $this->defaultData($locale);
    	$history = History::select('id',$locale.'_title as title', $locale.'_content as content')->get();
    	return view('frontend.about-us.history',['locale'=>$locale,'defaultData'=>$defaultData,'history'=>$history]);
    }
}
