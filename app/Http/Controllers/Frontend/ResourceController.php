<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\Faq;
use App\Model\Category;
use App\Model\Content;
use App\Model\Banner;
class ResourceController extends FrontendController
{

     public function guideline($locale="en"){
        $banner = Banner::select('image')->find(15);
        $defaultData = $this->defaultData($locale);
    	return view('frontend.resource.guideline-proposal',['defaultData'=>$defaultData,'locale'=>$locale,'banner'=>$banner]);
    }

    public function selection($locale="en"){
        $defaultData = $this->defaultData($locale);
        $banner = Banner::select('image')->find(15);
        $data = Content::select($locale.'_title as title', $locale.'_content as content', 'image')
		->where('slug', 'selection-criteria ')
		->first();
    	return view('frontend.resource.selection',['defaultData'=>$defaultData,'locale'=>$locale, 'data' =>$data,'banner'=>$banner]);
    }

    public function faq($locale="en"){
        $defaultData = $this->defaultData($locale);
        $banner = Banner::select('image')->find(15);
        $faqs = Category::select('id',$locale.'_title as title')
        ->with(['questions'=> function($q) use ($locale){
            $q->select('id', 'category_id' ,'is_published', $locale.'_question as question', $locale.'_content as answer')->where('is_published',1); 
        }])
        ->get();
        
        return view('frontend.resource.faq',
        ['defaultData'=>$defaultData,
        'locale'=>$locale,
         'faqs' => $faqs,
         'banner'=>$banner
         ]);
    }


}
