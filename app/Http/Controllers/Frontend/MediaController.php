<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\Media;
use App\Model\Banner;
class MediaController extends FrontendController
{

    public function listing($locale="en"){
        $defaultData = $this->defaultData($locale);
        $banner = Banner::select('id','is_published','image')->find(14);
        $data = Media::select('id',$locale.'_title as title',$locale.'_content as content','image')->get();
         
        return view('frontend.media.media',
            ['locale'=>$locale,
            'defaultData' => $defaultData,'data' => $data,'banner'=>$banner]);
    }
}
