<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\support\Facades\Route;
use App\Model\Slide as Slide;
use App\Model\News as News;


class FrontendController extends Controller
{
    public $defaultData =array();
    	public function __construct(){

    	}
    	public function defaultData($locale = "en"){
			
      		App::setLocale($locale);
        //Current Language
    		$parameters = Route::getCurrentRoute()->parameters();

    		$enRouteParameters = $parameters;
    		$enRouteparemeters['locale'] = 'en';
    		$this->defaultData['enRouteParameters'] = $enRouteparemeters;

    		$khRouteParameters = $parameters;
    		$khRouteParameters['locale'] = 'kh';
    		$this->defaultData['khRouteParameters'] =$khRouteParameters;

    		$this->defaultData['routeName'] = Route::currentRouteName();


    		// $this->defaultData['director-speech'] = Links::select('id', $locale.'_title as title' $locale.'_content as content','image')->where('is_published', 1)->get();

    		return $this->defaultData;
    	}
   
}
