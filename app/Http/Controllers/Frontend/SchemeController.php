<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\Content;
use App\Model\Banner;
class SchemeController extends FrontendController
{

    public function largeFirm($locale="en"){
		$defaultData = $this->defaultData($locale);
		
		$banner = Banner::select('image','is_published')->find(13);
		$data   = Content::select('id',$locale.'_title as title',$locale.'_content as content')
		->where('slug','large-firm')
		->first();
		
    	return view('frontend.scheme.large-firm',['defaultData'=>$defaultData,'locale'=>$locale,'data'=>$data,'banner'=>$banner]);
	}

	public function sme($locale="en"){
		$defaultData = $this->defaultData($locale);

		$banner = Banner::select('image','is_published')->find(13);
		$data=Content::select('id',$locale.'_title as title',$locale.'_content as content')
		->where('slug','sme')
		->first();
		
    	return view('frontend.scheme.sme',['defaultData'=>$defaultData,'locale'=>$locale,'data'=>$data,'banner'=>$banner]);
	}

	public function trainer($locale="en"){
		$defaultData = $this->defaultData($locale);
		
		$banner = Banner::select('image','is_published')->find(13);
		$data=Content::select('id',$locale.'_title as title',$locale.'_content as content')
		->where('slug','training-of-trainer')
		->first();
		
    	return view('frontend.scheme.training-of-trainer',['defaultData'=>$defaultData,'locale'=>$locale,'data'=>$data,'banner'=>$banner]);
	}
	
}
