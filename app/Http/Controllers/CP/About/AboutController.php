<?php

namespace App\Http\Controllers\CP\About;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Session;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;


use App\Model\Content;


class AboutController extends Controller
{
    
    function __construct (){
       $this->route = "cp.about.director-speech";
    }

    public function update(Request $request, $slug=""){   
     
     
     
      $data = array( 
                  'kh_content_part1' =>   $request->input('kh_content_part1'),
                  'en_content_part1' =>   $request->input('en_content_part1'),
                  'kh_content_part2' =>   $request->input('kh_content_part2'),
                  'en_content_part2' =>   $request->input('en_content_part2'),
                    // 'en_content' =>   $request->input('en_content'),
                    // 'kh_content' =>   $request->input('kh_content'),
                );

      
      // Validator::make($request->all(), $validate)->validate();
  
   
      Content::where('slug', $slug)->update($data);

      Session::flash('msg', 'Data has been updated!' );
      return redirect()->back(); 
    }

  

    public function view($slug=''){ 
      $data = Content::select('*')
      ->where('slug', $slug)
      ->first();

    
     

      if($data){
        return view('cp.about.edit', ['route'=>$this->route,'data'=>$data]);
      }else{
        return response(view('errors.404'), 404);
      }
    }
   
}
