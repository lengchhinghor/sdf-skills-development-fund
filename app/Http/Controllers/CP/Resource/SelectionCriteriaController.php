<?php

namespace App\Http\Controllers\CP\Resource;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Session;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;


use App\Model\Content;


class SelectionCriteriaController extends Controller
{
    
    function __construct (){
       $this->route = "cp.resource.selection-criteria";
    }

    public function update(Request $request){   
      $id = $request->input('id');
      $image = "";
     
      $data = array( 
                   
                    'en_content' =>   $request->input('en_content'),
                    'kh_content' =>   $request->input('kh_content'),
                );

      
      // Validator::make($request->all(), $validate)->validate();
  
   
      Content::where('slug', 'selection-criteria')->update($data);

      Session::flash('msg', 'Data has been updated!' );
      return redirect()->back(); 
    }

  

    public function view(){ 

      $data = Content::select('*')
      ->where('slug', 'selection-criteria')
      ->first();

      if(!empty($data)){
        return view('cp.resource.selection-criteria.edit', ['route'=>$this->route,'data'=>$data]);
      }else{
        return response(view('errors.404'), 404);
      }
    }
   
}
