<?php

namespace App\Http\Controllers\CP\Scheme;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Session;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;


use App\Model\Content;


class SchemeController extends Controller
{
    
    function __construct (){
       $this->route = "cp.scheme.large-firm";
    }

    public function update(Request $request, $slug=""){   
     
     
     
      $data = array( 
                   
                    'en_content' =>   $request->input('en_content'),
                    'kh_content' =>   $request->input('kh_content'),
                );

      
      // Validator::make($request->all(), $validate)->validate();
  
   
      Content::where('slug', $slug)->update($data);

      Session::flash('msg', 'Data has been updated!' );
      return redirect()->back(); 
    }

  

    public function view($slug=''){ 
      $data = Content::select('*')
      ->where('slug', $slug)
      ->first();

    
     

      if($data){
        return view('cp.scheme.edit', ['route'=>$this->route,'data'=>$data]);
      }else{
        return response(view('errors.404'), 404);
      }
    }
   
}
