<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
   
    protected $table = 'message';

    public function career(){
        return $this->belongsTo('App\Model\Career');
    }
    
}
