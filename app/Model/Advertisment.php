<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Advertisment extends Model
{
   
    protected $table = 'advertisments';
   
    public function type(){
        return $this->belongsTo('App\Model\Type', 'type_id');
    }
}
