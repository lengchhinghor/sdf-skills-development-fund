<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
   
    protected $table = 'category';
   
    public function questions(){
        return $this->hasMany('App\Model\Faq', 'category_id');
    }
}
