<?php

Route::get('/', 				['as' => 'index', 			'uses' => 'PrioritySectorController@index']);
Route::get('/create', 			['as' => 'create', 			'uses' => 'PrioritySectorController@create']);
Route::put('/', 				['as' => 'store', 			'uses' => 'PrioritySectorController@store']);
Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'PrioritySectorController@showEditForm']);
Route::post('/', 				['as' => 'update', 			'uses' => 'PrioritySectorController@update']);

Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'PrioritySectorController@trash']);
Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'PrioritySectorController@updateStatus']);
