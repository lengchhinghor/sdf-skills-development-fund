<?php

	//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Auth
	Route::group(['as' => 'auth.', 'prefix' => 'auth', 'namespace' => 'Auth'], function(){	
		require(__DIR__.'/auth.php');
	});
	
	//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Authensicated
	Route::group(['middleware' => 'authenticatedUser'], function() {
		Route::group(['as' => 'user.',  'prefix' => 'user', 'namespace' => 'User'], function () {
			require(__DIR__.'/user.php');
		});		
		Route::group(['as' => 'image.',  'prefix' => 'image', 'namespace' => 'Image'], function () {
			require(__DIR__.'/image.php');
		});
		Route::group(['as' => 'slide.',  'prefix' => 'slide', 'namespace' => 'Slide'], function () {
			require(__DIR__.'/slide.php');
		});
		Route::group(['as' => 'banner.',  'prefix' => 'banner', 'namespace' => 'Banner'], function () {
			require(__DIR__.'/banner.php');
		});
		Route::group(['as' => 'about.',  'prefix' => 'about', 'namespace' => 'About'], function () {
			require(__DIR__.'/about.php');
		});
		Route::group(['as' => 'director-speech.',  'prefix' => 'director-speech', 'namespace' => 'DirectorSpeech'], function () {
			require(__DIR__.'/director-speech.php');
		});
		Route::group(['as' => 'organization.',  'prefix' => 'organization', 'namespace' => 'Organization'], function () {
			require(__DIR__.'/organization.php');
		});

		Route::group(['as' => 'new-event.',  'prefix' => 'new-event', 'namespace' => 'NewEvent'], function () {
			require(__DIR__.'/new-event.php');
		});

		Route::group(['as' => 'priority-sector.',  'prefix' => 'priority-sector', 'namespace' => 'PrioritySector'], function () {
			require(__DIR__.'/priority-sector.php');
		});

		Route::group(['as' => 'partner.',  'prefix' => 'partner', 'namespace' => 'Partner'], function () {
			require(__DIR__.'/partner.php');
		});

		Route::group(['as' => 'training-large.',  'prefix' => 'training-large', 'namespace' => 'TrainingLarge'], function () {
			require(__DIR__.'/training-large.php');
		});

		Route::group(['as' => 'training-sms.',  'prefix' => 'training-sms', 'namespace' => 'TrainingSMS'], function () {
			require(__DIR__.'/training-sms.php');
		});

		Route::group(['as' => 'training-trainer.',  'prefix' => 'training-trainer', 'namespace' => 'TrainingTrainer'], function () {
			require(__DIR__.'/training-trainer.php');
		});

		Route::group(['as' => 'career.',  'prefix' => 'career', 'namespace' => 'Career'], function () {
			require(__DIR__.'/career.php');
		});

		Route::group(['as' => 'event.',  'prefix' => 'event', 'namespace' => 'Event'], function () {
			require(__DIR__.'/event.php');
		});

		Route::group(['as' => 'announcement.',  'prefix' => 'announcement', 'namespace' => 'Announcement'], function () {
			require(__DIR__.'/announcement.php');
		});

		Route::group(['as' => 'event-new.',  'prefix' => 'event-new', 'namespace' => 'EventNew'], function () {
			require(__DIR__.'/event-new.php');
		});

		Route::group(['as' => 'application.',  'prefix' => 'application', 'namespace' => 'Application'], function () {
			require(__DIR__.'/application.php');
		});

		Route::group(['as' => 'news.',  'prefix' => 'news', 'namespace' => 'News'], function () {
			require(__DIR__.'/news.php');
		});
	
		Route::group(['as' => 'category.',  'prefix' => 'category', 'namespace' => 'Category'], function () {
			require(__DIR__.'/category.php');
		});

		Route::group(['as' => 'training-project.',  'prefix' => 'training-project', 'namespace' => 'TrainingProject'], function () {
			require(__DIR__.'/training-project.php');
		});

		Route::group(['as' => 'massage.',  'prefix' 		=> 'massage', 'namespace' 	   => 'Massage'], function () {
			require(__DIR__.'/massage.php');
		});
		Route::group(['as' => 'message.',  'prefix' 		=> 'message', 'namespace' 	   => 'Message'], function () {
			require(__DIR__.'/message.php');
		});

		Route::group(['as' => 'scheme.',  'prefix' => 'scheme', 'namespace' => 'Scheme'], function () {
			require(__DIR__.'/scheme.php');
		});

		Route::group(['as' => 'faq.',  'prefix' => 'faq', 'namespace' => 'Faq'], function () {
			require(__DIR__.'/faq.php');
		});

		Route::group(['as' => 'resource.',  'prefix' => 'resource', 'namespace' => 'Resource'], function () {
			require(__DIR__.'/resource.php');
		});

		Route::group(['as' => 'media.',  'prefix' => 'media', 'namespace' => 'Media'], function () {
			require(__DIR__.'/media.php');
		});
	});