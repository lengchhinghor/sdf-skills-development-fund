
<?php

Route::get('/', 				['as' => 'index', 			'uses' => 'TrainingProjectController@index']);
Route::get('/create', 			['as' => 'create', 			'uses' => 'TrainingProjectController@create']);
Route::put('/', 				['as' => 'store', 			'uses' => 'TrainingProjectController@store']);
Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'TrainingProjectController@showEditForm']);
Route::post('/', 				['as' => 'update', 			'uses' => 'TrainingProjectController@update']);

Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'TrainingProjectController@trash']);
Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'TrainingProjectController@updateStatus']);
