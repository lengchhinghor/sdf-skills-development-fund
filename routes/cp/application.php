<?php

Route::get('/', 				['as' => 'index', 			'uses' => 'ApplicationController@index']);
Route::get('/create', 			['as' => 'create', 			'uses' => 'ApplicationController@create']);
Route::put('/', 				['as' => 'store', 			'uses' => 'ApplicationController@store']);
Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'ApplicationController@showEditForm']);
Route::post('/', 				['as' => 'update', 			'uses' => 'ApplicationController@update']);

Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'ApplicationController@trash']);
Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'ApplicationController@updateStatus']);
Route::post('/order', 			['as' => 'order', 			'uses' => 'ApplicationController@order']);
