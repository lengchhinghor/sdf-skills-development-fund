<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'ServiceController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'ServiceController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'ServiceController@update']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'ServiceController@create']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'ServiceController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'ServiceController@trash']);

});	