
<?php

Route::get('/', 				['as' => 'index', 			'uses' => 'TrainingLargeController@index']);
Route::get('/create', 			['as' => 'create', 			'uses' => 'TrainingLargeController@create']);
Route::put('/', 				['as' => 'store', 			'uses' => 'TrainingLargeController@store']);
Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'TrainingLargeController@showEditForm']);
Route::post('/', 				['as' => 'update', 			'uses' => 'TrainingLargeController@update']);

Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'TrainingLargeController@trash']);
Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'TrainingLargeController@updateStatus']);
