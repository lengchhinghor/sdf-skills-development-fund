
<?php

Route::get('/', 				['as' => 'index', 			'uses' => 'TrainingTrainerController@index']);
Route::get('/create', 			['as' => 'create', 			'uses' => 'TrainingTrainerController@create']);
Route::put('/', 				['as' => 'store', 			'uses' => 'TrainingTrainerController@store']);
Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'TrainingTrainerController@showEditForm']);
Route::post('/', 				['as' => 'update', 			'uses' => 'TrainingTrainerController@update']);

Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'TrainingTrainerController@trash']);
Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'TrainingTrainerController@updateStatus']);
