<?php

Route::get('/{slug}', 				['as' => 'view', 			'uses' => 'SchemeController@view']);
Route::post('/{slug}', 				['as' => 'update', 			'uses' => 'SchemeController@update']);

