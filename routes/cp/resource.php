<?php

Route::get('/{slug}', 				['as' => 'view', 			'uses' => 'ResourceController@view']);
Route::post('/{slug}', 				['as' => 'update', 			'uses' => 'ResourceController@update']);

// Route::group(['as' => 'history.',  'prefix' => 'history'], function () {
//     Route::get('/', 				['as' => 'view', 			'uses' => 'HistoryController@view']);
//     Route::post('/', 				['as' => 'update', 			'uses' => 'HistoryController@update']);
// });

// Route::group(['as' => 'speech.',  'prefix' => 'speech'], function () {
//     Route::get('/', 				['as' => 'view', 			'uses' => 'SpeechController@view']);
//     Route::post('/', 				['as' => 'update', 			'uses' => 'SpeechController@update']);
// });

