<?php

Route::get('/', 				['as' => 'index', 			'uses' => 'PartnerController@index']);
Route::get('/create', 			['as' => 'create', 			'uses' => 'PartnerController@create']);
Route::put('/', 				['as' => 'store', 			'uses' => 'PartnerController@store']);
Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'PartnerController@showEditForm']);
Route::post('/', 				['as' => 'update', 			'uses' => 'PartnerController@update']);

Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'PartnerController@trash']);
Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'PartnerController@updateStatus']);
