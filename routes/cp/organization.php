<?php

Route::get('/', 				['as' => 'index', 			'uses' => 'OrganizationController@index']);
Route::get('/create', 			['as' => 'create', 			'uses' => 'OrganizationController@create']);
Route::put('/', 				['as' => 'store', 			'uses' => 'OrganizationController@store']);
Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'OrganizationController@showEditForm']);
Route::post('/', 				['as' => 'update', 			'uses' => 'OrganizationController@update']);

Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'OrganizationController@trash']);
Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'OrganizationController@updateStatus']);
