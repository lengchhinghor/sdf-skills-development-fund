<?php

Route::get('/', 				['as' => 'index', 			'uses' => 'MediaController@index']);
Route::get('/create', 			['as' => 'create', 			'uses' => 'MediaController@create']);
Route::put('/', 				['as' => 'store', 			'uses' => 'MediaController@store']);
Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'MediaController@showEditForm']);
Route::post('/', 				['as' => 'update', 			'uses' => 'MediaController@update']);

Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'MediaController@trash']);
Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'MediaController@updateStatus']);
Route::post('/order', 			['as' => 'order', 			'uses' => 'MediaController@order']);
