<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Faq

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 				'uses' => 'FaqController@index']);
	Route::get('/feature', 			['as' => 'feature', 			'uses' => 'FaqController@feature']);
	Route::get('/create', 			['as' => 'create', 				'uses' => 'FaqController@create']);
	Route::put('/', 				['as' => 'store', 				'uses' => 'FaqController@store']);
	Route::get('/{id}', 			['as' => 'edit', 				'uses' => 'FaqController@edit']);
	Route::post('/', 				['as' => 'update', 				'uses' => 'FaqController@update']);
	
	Route::delete('/{id}', 			['as' => 'trash', 				'uses' => 'FaqController@trash']);
	Route::post('status', 			['as' => 'update-status', 		'uses' => 'FaqController@updateStatus']);
	Route::post('/order', 			['as' => 'order', 				'uses' => 'FaqController@order']);
	Route::post('update-featured', 	['as' => 'update-featured', 	'uses' => 'FaqController@updateFeatured']);
	
});	