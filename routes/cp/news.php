<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> News

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 				'uses' => 'NewsController@index']);
	Route::get('/feature', 			['as' => 'feature', 			'uses' => 'NewsController@feature']);
	Route::get('/creates', 			['as' => 'create', 				'uses' => 'NewsController@create']);
	Route::put('/', 				['as' => 'store', 				'uses' => 'NewsController@store']);
	Route::get('/{id}', 			['as' => 'edit', 				'uses' => 'NewsController@edit']);
	Route::post('/', 				['as' => 'update', 				'uses' => 'NewsController@update']);
	
	Route::delete('/{id}', 			['as' => 'trash', 				'uses' => 'NewsController@trash']);
	Route::post('status', 			['as' => 'update-status', 		'uses' => 'NewsController@updateStatus']);
	Route::post('/order', 			['as' => 'order', 				'uses' => 'NewsController@order']);
	Route::post('update-featured', 	['as' => 'update-featured', 	'uses' => 'NewsController@updateFeatured']);
	
});	