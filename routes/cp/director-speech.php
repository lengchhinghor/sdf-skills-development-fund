<?php

Route::get('/', 				['as' => 'index', 			'uses' => 'DirectorSpeechController@index']);
Route::get('/create', 			['as' => 'create', 			'uses' => 'DirectorSpeechController@create']);
Route::put('/', 				['as' => 'store', 			'uses' => 'DirectorSpeechController@store']);
Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'DirectorSpeechController@showEditForm']);
Route::post('/', 				['as' => 'update', 			'uses' => 'DirectorSpeechController@update']);

Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'DirectorSpeechController@trash']);
Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'DirectorSpeechController@updateStatus']);
