<?php

Route::get('/{slug}', 				['as' => 'view', 			'uses' => 'AboutController@view']);
Route::post('/{slug}', 				['as' => 'update', 			'uses' => 'AboutController@update']);

// Route::group(['as' => 'history.',  'prefix' => 'history'], function () {
//     Route::get('/', 				['as' => 'view', 			'uses' => 'HistoryController@view']);
//     Route::post('/', 				['as' => 'update', 			'uses' => 'HistoryController@update']);
// });

// Route::group(['as' => 'speech.',  'prefix' => 'speech'], function () {
//     Route::get('/', 				['as' => 'view', 			'uses' => 'SpeechController@view']);
//     Route::post('/', 				['as' => 'update', 			'uses' => 'SpeechController@update']);
// });

