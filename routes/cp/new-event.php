<?php

Route::get('/', 				['as' => 'index', 			'uses' => 'NewEventController@index']);
Route::get('/create', 			['as' => 'create', 			'uses' => 'NewEventController@create']);
Route::put('/', 				['as' => 'store', 			'uses' => 'NewEventController@store']);
Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'NewEventController@showEditForm']);
Route::post('/', 				['as' => 'update', 			'uses' => 'NewEventController@update']);

Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'NewEventController@trash']);
Route::post('update-status', 	['as' => 'update-status', 	'uses' => 'NewEventController@updateStatus']);
