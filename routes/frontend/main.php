<?php
Route::get('{locale}/home',							['as' => 'home',				'uses' => 'HomeController@index']);

Route::get('{locale}/about/director-speech',        ['as' => 'director-speech',    	'uses' => 'AboutController@directorSpeech']);
Route::get('{locale}/about/history',                ['as' => 'history',    	        'uses' => 'AboutController@history']);
Route::get('{locale}/about/organization',           ['as' => 'organization',    	'uses' => 'AboutController@organization']);

Route::get('{locale}/scheme/large-firm',            ['as' => 'large-firm',    	    'uses' => 'SchemeController@largeFirm']);
Route::get('{locale}/scheme/sme',                   ['as' => 'sme',    	            'uses' => 'SchemeController@sme']);
Route::get('{locale}/scheme/training-of-trainer',   ['as' => 'training-of-trainer', 'uses' => 'SchemeController@trainer']);

Route::get('{locale}/resource/selection',           ['as' => 'selection',    	    'uses' => 'ResourceController@selection']);
Route::get('{locale}/resource/guideline-proposal',  ['as' => 'guideline-proposal',  'uses' => 'ResourceController@guideline']);
Route::get('{locale}/resource/faq',                 ['as' => 'faq',  				'uses' => 'ResourceController@faq']);

Route::get('{locale}/posts',		                ['as' => 'news',    			'uses' => 'NewsController@listing']);
Route::get('{locale}/posts/{id}',		            ['as' => 'news-detail',    		'uses' => 'NewsController@read']);


Route::get('{locale}/events',		                ['as' => 'event',    			'uses' => 'EventController@listing']);
Route::get('{locale}/announcements',		        ['as' => 'announcement',    	'uses' => 'AnnouncementController@listing']);

Route::get('{locale}/career',		                ['as' => 'career',    	        'uses' => 'CareerController@listing']);
Route::get('{locale}/career/{id}',	                ['as' => 'career-detail',       'uses' => 'CareerController@read']);

Route::get('{locale}/contact-us',		            ['as' => 'contact-us',    		'uses' => 'ContactController@index']);
Route::put('/sendmessage', 							['as' => 'sendmessage',			'uses' => 'ContactController@sendMassage']);
Route::put('/send-career', 					        [ 'as' =>'send-career',		    'uses' => 'CareerController@sendMessage']);

Route::get('{locale}/get-application', 			    [ 'as' => 'get-application',    'uses' => 'HomeController@getApplication']);
Route::get('{locale}/media',		                ['as' => 'media',    			'uses' => 'MediaController@listing']);