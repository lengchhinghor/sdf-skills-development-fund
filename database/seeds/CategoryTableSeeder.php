<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('category')->insert(
            [
                [
                    'en_title'              => 'សេចក្តីជូនដំណឹង។',
                ],
                [
                    'en_title'              => 'អាជីព',
                ],

                [
                    'en_title'              => 'ព្រឹត្តិការណ៍',
                ],

                [
                    'en_title'              => 'ព័ត៌មាន',
                ],
            ]
        );
	}
}
