<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareerApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('career_application', function (Blueprint $table) {
            $table->increments('id', 11);
            
            $table->integer('data_order')->nullable();
            $table->integer('career_id')->nullable();
            $table->string('name', 500)->nullable();
            $table->text('en_description')->nullable();
            $table->text('email')->nullable();
            $table->text('phone')->nullable();
            $table->text('cv')->nullable();
            //The field that will appear for almost tables
            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('career_application');
    }
}
